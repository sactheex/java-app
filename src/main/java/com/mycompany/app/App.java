package com.mycompany.app;

/**
 * Hello world!
 */
public class App
{

    /**   Linked to @wi.implements R03-588
    new function implemented according to the task description created in Polarion
    */  	 
    private final String message = "Hello World!";
    public App() {}

	/**   Linked to @wi.implements R03-594
    new function implemented according to task in Polarion
    implement wlan connections changes are done
    */  
    public static void main(String[] args) {
        System.out.println(new App().getMessage());
    }

    //here is an additional function in my source code

	/**   Linked to @wi.implements R03-751
    new function implemented according to task in Polarion
    new code implemented
    */  	 
    private final String getMessage() {
        String implementation = "Here takes the software implementation place";
        return message;
    }

// added a new comment

    /**   Linked to @wi.implements A079-1924
	 all required safety features implemented
     regulatory requirments are considered and tested
     all additional security functions developed
    */  	 
    private final String getSafetyMessage() {
        String safety = "Safety function is implemented";
        return safety;
    }

    /**   Linked to Polarion Item : @wi.implements A079-1905
	 all bluetooth security functions described in Polarion task are implemented and tested successfully
     added new security protocol
     */  	 
    private final String getSomeSafetyMessage() {
        String safety = "All functions implemented"; 
        return safety;
    }

}
